ioboard
=======

Firmware for snap-stack io board.

Installation:
- Install Arduino IDE for Linux, version 1.8.13
    - Note you must compile this repository on Linux due to symlinking
    - Don't use the latest version, as Teensy support lags the Arduino IDE, and the Teensy plugin does a version check
    - Use directions here: https://www.arduino.cc/en/guide/linux
- Install TeensyDuino for Linux
    - Directions here: https://www.pjrc.com/teensy/td_download.html
    - Point it at the Arduino IDE directory to which you installed above
    - The installer may get confused about the `udev` rules, because the directions call the file `00-teensy.rules` while the installer checks for `49-teensy.rules` or something like that.
- Install `ioboard`
    - `git clone git@gitlab.com:mit-acl/creare-labdrone/ioboard.git`
    - `cd ioboard`
    - `submodule update --init --recursive`
    - `arduino firmware/firmware.ino` (or open the sketch from the Open menu in the IDE)
    - Click "Tools", and confirm the board is set to Teensy 4.0
    - Plug in the Teensy via micro USB cable
    - Click the checkbox called "verify" to compile
    - The Teensy loader window should pop up; follow its directions to upload
