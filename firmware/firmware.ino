/**
 * @file firmware.ino
 * @brief Teensy 4.0 firmware for snap io board
 * @author Parker Lusk <plusk@mit.edu>
 * @date 14 Apr 2021
 */

#include "snapio-protocol/sio_serial.h"

//=============================================================================
// pin definitions
//=============================================================================

static constexpr int LED_PIN = 13; // on-board teensy LED

// pwm pins for motors
static constexpr int PWM_PINS[] = { 2, 3, 4, 5, 6, 7 }; // in motor order
static constexpr int NUM_PWM = 6; // number of pwm pins

// gpio pin for linear actuator (gripper)
static constexpr int GRIPPER_PIN = 8;

//=============================================================================
// configuration options
//=============================================================================

static constexpr int BAUD = 921600; // baud for snap <-> snapio comms

static constexpr int PWM_RESOLUTION = 15;
static constexpr int PWM_FREQ = 490;

static constexpr int COMM_TIMEOUT_USEC = 500000; // saftey stop on comm timeout

//=============================================================================
// global variables
//=============================================================================

// serial stuff
sio_serial_message_t msg_buf;

uint32_t last_msg_time = 0;

//=============================================================================
// Helper functions
//=============================================================================

/**
 * @brief      Convert PWM usec to duty cycle
 *
 * @param[in]  usec  PWM microseconds, 1000 <= usec <= 2000
 *
 * @return     0 <= dutycycle <= 2^PWM_RESOLUTION-1
 */
uint16_t usec2dutycycle(uint16_t usec)
{
  return ((2 << (PWM_RESOLUTION-1))-1) * usec * 1e-6 * PWM_FREQ;
}

// ----------------------------------------------------------------------------

void stopMotors()
{
  for (int i=0; i<NUM_PWM; i++) {
    analogWrite(PWM_PINS[i], usec2dutycycle(1000));
  }
}

//=============================================================================
// initialize
//=============================================================================

void setup()
{
  // UART from snapdragon for motor cmds (pins 0, 1)
  Serial1.begin(BAUD);

  // Setup led pin
  pinMode(LED_PIN, OUTPUT);

  // setup PWM pins to be 490 Hz and initialize to 1000 usec (motors off)
  analogWriteResolution(PWM_RESOLUTION);
  for (int i=0; i<NUM_PWM; i++) {
    analogWriteFrequency(PWM_PINS[i], PWM_FREQ);
    analogWrite(PWM_PINS[i], usec2dutycycle(1000));
  }

  // setup gripper pin
  pinMode(GRIPPER_PIN, OUTPUT);
}

//=============================================================================
// loop
//=============================================================================

void loop()
{
  if ((micros() - last_msg_time) > COMM_TIMEOUT_USEC) {
    stopMotors();
    digitalWriteFast(LED_PIN, HIGH);
  } else {
    digitalWriteFast(LED_PIN, LOW);
  }
}

//=============================================================================
// handle received serial data (from autopilot)
//=============================================================================

void serialEvent1()
{
  while (Serial1.available()) {
    uint8_t in_byte = (uint8_t) Serial1.read();
    if (sio_serial_parse_byte(in_byte, &msg_buf)) {
      switch (msg_buf.type) {
        case SIO_SERIAL_MSG_MOTORCMD:
        {
          sio_serial_motorcmd_msg_t msg;
          sio_serial_motorcmd_msg_unpack(&msg, &msg_buf);
          handle_motorcmd_msg(msg);
          break;
        }
        case SIO_SERIAL_MSG_GRIPPERCMD:
        {
          sio_serial_grippercmd_msg_t msg;
          sio_serial_grippercmd_msg_unpack(&msg, &msg_buf);
          handle_grippercmd_msg(msg);
          break;
        }
      }
    }
  }
}

//=============================================================================
// handle received messages
//=============================================================================

void handle_motorcmd_msg(const sio_serial_motorcmd_msg_t& msg)
{
  last_msg_time = micros();
  for (int i=0; i<NUM_PWM; i++) {
    analogWrite(PWM_PINS[i], usec2dutycycle(msg.usec[i]));
  }
}

// ----------------------------------------------------------------------------

void handle_grippercmd_msg(const sio_serial_grippercmd_msg_t& msg)
{
  digitalWriteFast(GRIPPER_PIN, ((msg.on) ? HIGH : LOW));
}
